import P from "../components/P";

const Home = (): JSX.Element => {
  return (
    <>
      <P>
        Test paragraph
      </P>
      <P adaptive>
        Test paragraph
      </P>
    </>
  );
};

export default Home;