import { PProps } from "./P.props";
import styles from './P.module.scss';
import classNames from 'classnames';

const P = ({ className, children, adaptive = false, ...props }: PProps): JSX.Element => {
  return (
    <p className={classNames(
      className,
      styles.primary,
      {
        [styles.adaptive]: adaptive,
      }
    )} {...props}>
      { children }
    </p>
  );
};

export default P;